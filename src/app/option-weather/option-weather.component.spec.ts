import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionWeatherComponent } from './option-weather.component';

describe('OptionWeatherComponent', () => {
  let component: OptionWeatherComponent;
  let fixture: ComponentFixture<OptionWeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionWeatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
