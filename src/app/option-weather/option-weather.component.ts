import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-option-weather',
  templateUrl: './option-weather.component.html',
  styleUrls: ['./option-weather.component.css']
})
export class OptionWeatherComponent implements OnInit {
widget: any = [{ name: 'Weather', city: 'London'}];
weather: any;

constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
  }

  getCity(city: string) {
    this.weatherService.getWeatherByCity(city)
    .subscribe((data: any) => {
        this.weather = data;
      }, (error) => {
        alert('Oops! Something went wrong. please verify your input.');
      }
    );
  }
  currentTime: number = Date.now();
}


