import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WeatherWidgetComponent } from './weather-widget/weather-widget.component';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  url = 'https://api.openweathermap.org/data/2.5/weather';
  apiKey = 'c937e57f947fc3b374274e8e78ee1521';
  units = 'metric';

  constructor(private http: HttpClient) { }

  getWeatherByCity(city: string) {
    let params = new HttpParams()
    .set('q', city)
    .set('units', 'metric')
    .set('appid', this.apiKey)

    return this.http.get(this.url, { params });
  } 
  
  openWeatherWidget: WeatherWidgetComponent[] = [];

  addWeather(openWeatherWidget: any) {
    this.openWeatherWidget.push(openWeatherWidget);
  }
}
