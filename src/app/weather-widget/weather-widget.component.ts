import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { DatePipe } from '@angular/common';
import { getAutomaticTypeDirectiveNames } from 'typescript';

@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.css']
})
export class WeatherWidgetComponent implements OnInit {
  weather: any;
  widgets: any = [{ name: 'Weather', city: 'London'}];
  isShown: boolean = false;
  
  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
  }

  getCity(city: string) {
    this.isShown = !this.isShown;
    this.weatherService.getWeatherByCity(city)
    .subscribe((data: any) => {
        this.weather = data;
      }, (error) => {
        alert('Oops! Something went wrong. please verify your input.');
      }
    );
  }
  currentTime: number = Date.now();
}
