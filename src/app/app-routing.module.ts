import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OptionWeatherComponent } from './option-weather/option-weather.component';
import { WeatherWidgetComponent } from './weather-widget/weather-widget.component';

const routes: Routes = [
  { path: 'weather-widget', component: WeatherWidgetComponent },
  { path: 'option-widget', component: OptionWeatherComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
